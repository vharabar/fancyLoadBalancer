package endpoints

import (
	"fancyLoadBalancer/myutil"
	"fmt"
	"log"
	"net/http"
)

type httpEndpoint struct {
	name     string
	bind     string
	CertFile string
	KeyFile  string
	server   http.Handler
}

func newHTTPEndpoint(conf *EndpointConfig) (Endpoint, error) {
	log.Printf("Creating HTTP endpoint %s", conf.Name)
	if conf.Router == nil {
		return nil, fmt.Errorf("missing Router object")
	}
	ha := &httpHeaderAdder{conf.Router}

	return &httpEndpoint{conf.Name, conf.Bind, conf.CertFile, conf.KeyFile, ha}, nil
}

func init() {
	Factories["HTTP"] = newHTTPEndpoint
}

// Added headers for connection
type httpHeaderAdder struct {
	server http.Handler
}

func (dg httpHeaderAdder) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	req.URL.Scheme = "http"
	req.Header.Add("X-Real-IP", myutil.RealIP(req))
	dg.server.ServeHTTP(rw, req)
}

func (htp *httpEndpoint) Listen() {

	log.Printf("Endpoint %s listening on %s", htp.name, htp.bind)
	err := http.ListenAndServe(htp.bind, htp.server)
	if err != nil {
		log.Fatalf("failed to bind httpEndpoint to %s: %s", htp.bind, err.Error())
	}
}
