package endpoints

import (
	"fancyLoadBalancer/myutil"
	"fmt"
	"log"
	"net/http"
)

type httpsEndpoint struct {
	name     string
	bind     string
	CertFile string
	KeyFile  string
	server   http.Handler
}

func newHTTPSEndpoint(conf *EndpointConfig) (Endpoint, error) {
	if len(conf.KeyFile) == 0 {
		return nil, (fmt.Errorf("missing keye file for https server"))
	}
	if len(conf.CertFile) == 0 {
		return nil, (fmt.Errorf("missing keye file for https server"))
	}
	log.Printf("Creating HTTPS endpoint %s", conf.Name)
	if conf.Router == nil {
		return nil, fmt.Errorf("missing router object")
	}
	ha := &httpsHeaderAdder{conf.Router}

	return &httpsEndpoint{conf.Name, conf.Bind, conf.CertFile, conf.KeyFile, ha}, nil
}

func init() {
	Factories["HTTPS"] = newHTTPSEndpoint
}

// Added headers for downgraded connection
type httpsHeaderAdder struct {
	server http.Handler
}

func (dg httpsHeaderAdder) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	req.URL.Scheme = "http"
	req.Header.Add("X-Forwarded-Proto", "https")
	req.Header.Add("X-Real-IP", myutil.RealIP(req))
	dg.server.ServeHTTP(rw, req)
}

func (hts *httpsEndpoint) Listen() {

	log.Printf("Endpoint %s listening on %s", hts.name, hts.bind)
	err := http.ListenAndServeTLS(hts.bind, hts.CertFile, hts.KeyFile, hts.server)
	if err != nil {
		log.Fatalf("failed to bind httpsEndpoint to %s: %s", hts.bind, err.Error())
	}
}
