# FancyLoadBalancer

##### This is a personal project I wrote while learning GoLang and experimenting with GitLab CI/CD features. The balancer is basic but functional. I tried my best to write modular code, so adding new features should be easy. If you're interested in this project you are welcome to participate: either create a new issue or merge request.

A very small and configurable load balancer that can handle 2 protocols:

 * `HTTP`
 * `HTTPS` (downgrades to http)

It also can use `http` redirects in case you want to point to different domain or upgrade client connection to `https`.

## To compile
    go get $(cat dependencies.txt | tr "\n" " ")
    go build -v

## To configure

Binary expects to find `config.yml` inside `config` directory in the same directory.
```sh
folder
 |\_ fancyLoadBalancer #compiled executable
  \_ config\
    \_ config.yml
```

## Using as docker image

Docker images are build automatically and can be pulled from:
`registry.gitlab.com/vharabar/fancyloadbalancer`  
There is no default configuration provided so you need to provide one at `/balancer/config/`**:**  
`docker run -d -v /path/to/config:/balancer/config`  
Don't forget to expose ports for incoming traffic

For `HTTPS` balancing a SSL certificate and key file are required.
Mount containing folder inside container and set the path inside `config.yml`


## `config.yml` format

This is short reference for configuration format

```yaml
name: string
endpoint: HTTP|HTTPS
bind: host:port
certificate: /path/to/cert
key: /path/to/key
routes:
  /url/path: servicename
redirects:
  /redirect/path: other/domain
services:
 serviceA:
  discovery: static|dns|docker-service
  autoUpdate: true|false
  updateInterval: int
  bstype: RoundRobin|SingleBackend
  backends:
   - hostA:portA
   - hostB:portB
```

- `name` is the name of current instsnce, replace it with desired value.
- `endpoint` is endpoint type, can have one of 2 values:
    - `HTTP`: accepts http traffic
    - `HTTPS`: accepts https traffic, requires a certificate to work
- `bind` is ip and port that balancer should listed to. format is `host`:`port`. `port` is required.
- `certificate` is location of ssl certificate file, is required for `https` balancer
- `keyfile` is also required for `https` balancer
- `routes` is a map of routes, each pointing to a specific service
    - `/url/path` - replace this with desired path (ex. "example.com/api" will me "/api")
    - `servicename` - replace this with name of your serice, must correspond to one of the services defined below. For external URLs (such as `google.com`) you must indicate complete URL including protocol (ex. `https://gooogle.com`)
- `redirects` uses `http` reidrect (code `307`) to redirect client to a different path, or different domain is protocol is specified (ex. `https://domain.example`)
    - `/redirect/path` - replace this with desired path (ex. "example.com/api" will me "/api")
    - `other/domain` - replace this with other path or other domain by indicating protocol
- `services` is a map of services that are served by this balancer
    - `serviceA` is the name of your serive, replace with desired name.
        - `discovery` is method by which backends are dicovered:
            - `list`: backend addresses are taken directly from `backends` list
            - `dns`: for each item in `backends` list will resolve it's ip adress using dns resolution
            - `docker-service`: same as dns, but will prepend `tasks.` to each entry.
        - `autoUpdate` defines whether or not backend list should be updated periodically. Either `true` or `false`, doesn't work for list.
        - `updateInterval` defines update interval in seconds, should be a positive integer.
        - `bstype` is backend selector type. Backend selector decides to which server the traffic should be rerouted. For now there are 2 implementations:
            - `RoundRobin`: classic balancing algorythm, selects each server in a row
            - `SingleBackend`: only serves a signle backend
        - `backends` is list of backends to balance, either list of ip addresse of dns names. If `port` in not indicated, `80` is assumed
![](test-config/config-stub.yml)
Example configurations can be found inside [test-config/config.yml](test-config/config.yml)