package myutil

import (
	"fmt"
	"log"
	"net"
	"net/http"
)

// RealIP returns real ip of a client
func RealIP(req *http.Request) string {
	host, _, _ := net.SplitHostPort(req.RemoteAddr)
	return host
}

// LookUp looks up IPs for a giver domain name
func LookUp(host string) (addrs []string, er error) {
	h, p, err := net.SplitHostPort(host)
	if err != nil {
		log.Printf("Port missing in %s, assumed 80", host)
		h, p = host, "80"
	}
	ips, err := net.LookupIP(h)
	if err != nil {
		er = fmt.Errorf("Could not resolve host %s: %s", h, err.Error())
		addrs = nil
		return
	}
	for _, ip := range ips {
		if ip4 := ip.To4(); ip4 != nil {
			addrs = append(addrs, ip.To4().String()+":"+p)
		}
	}
	er = nil
	return
}

// TODO: Add method to periodically update backend lists
// LookUpHosts resolves dns names of ulr's in list
func LookUpHosts(hs []string, prefix string) ([]string, error) {
	bes := make([]string, 0)
	for _, name := range hs {
		name = prefix + name
		a, err := LookUp(name)
		if err != nil {
			return nil, fmt.Errorf("Could not resolve %s, %s", name, err)
		}
		bes = append(bes, a...)
	}
	return bes, nil
}
