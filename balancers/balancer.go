// Package balancers provides load balancers for different protocols.
package balancers

import (
	bs "fancyLoadBalancer/backendSelectors"
	"net/http"
)

// Balancer will balance request between different backends
type Balancer interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
	UpdateBackends(backends []bs.Backend)
}

type BalancerConfig struct {
	Name, BalancerType string
	Backend            bs.BackendSelector
}

// BalancerFactory will produce a Balancer of a giver variety.
// `selectorFactory` is a factory that will be used to create appropriate BackendSelector.
type BalancerFactory func(*BalancerConfig) (Balancer, error)

// Factories is a map of factories that let you create
// a Balancer of a giver variety.
var Factories map[string]BalancerFactory

func init() {
	Factories = make(map[string]BalancerFactory, 0)
}

// Build build a balancer from a valid configuration
func Build(conf *BalancerConfig) (Balancer, error) {
	return Factories[conf.BalancerType](conf)
}
