package main

import (
	bs "fancyLoadBalancer/backendSelectors"
	bal "fancyLoadBalancer/balancers"
	ep "fancyLoadBalancer/endpoints"
	"fancyLoadBalancer/myutil"
	"fancyLoadBalancer/redirect"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"time"
)

// translate a list of domains into ip adresses
func lookUpHosts(hs []string, prefix string) ([]string, error) {
	belist := make([]string, 0)
	for _, name := range hs {
		name = prefix + name
		a, err := myutil.LookUp(name)
		if err != nil {
			return nil, fmt.Errorf("Could not resolve %s, %s", name, err)
		}
		belist = append(belist, a...)
	}
	return belist, nil
}

func buildBackend(name string, srv Service) (bs.BackendSelector, bs.BackendUpdater, error) {
	discovery := srv.Discovery
	var upd bs.BackendUpdater
	var belist []string
	var err error

	switch {
	case discovery == "list":
		belist = srv.Backends
	case discovery == "dns" || discovery == "docker-service":
		prefix := ""
		if discovery == "docker-service" {
			prefix = "tasks."
		}
		belist, err = lookUpHosts(srv.Backends, prefix)
		if err != nil {
			return nil, nil, err
		}
		if srv.AutoUpdate {
			if srv.UpdateInterval < 1 {
				return nil, nil, fmt.Errorf("If using AutoUpdade, update delay should be > 1")
			}
			bel := srv.Backends
			upd = func(belist bs.BackendSelector) {
				for {
					hl, err := lookUpHosts(bel, prefix)
					if err != nil {
						panic(err)
					}
					belist.Update(bs.Strings2Backends(hl))
					time.Sleep(time.Duration(srv.UpdateInterval) * time.Second)
					log.Printf("Updating backends for %s", name)
				}
			}
		}
	default:
		return nil, nil, fmt.Errorf("Bad value for Discovery in %s", name)
	}

	bsc := &bs.BackendSelectorConfig{srv.Bstype, bs.Strings2Backends(belist)}
	res, err := bs.Build(bsc)
	if err != nil {
		return nil, nil, err
	}

	return res, upd, nil
}

func chainFromConfig(cfg YamlConfig) (ep.Endpoint, error) {
	backends := make(map[string]bs.BackendSelector)

	for k, v := range cfg.Services {
		be, upd, err := buildBackend(k, v)
		if err != nil {
			return nil, err
		}
		backends[k] = be
		if upd != nil {
			go upd(be)
		}
	}

	router := http.NewServeMux()

	for route, service := range cfg.Routes {
		balancerConfig := &bal.BalancerConfig{
			Name:         service,
			BalancerType: "SimpleBalancer",
			Backend:      backends[service],
		}
		balancer, err := bal.Build(balancerConfig)
		if err != nil {
			return nil, err
		}
		log.Printf("Creating route from %s to %s", route, service)
		router.Handle(route, balancer)
	}

	for inc, out := range cfg.Redirects {
		outurl, err := url.Parse(out)
		if err != nil {
			return nil, fmt.Errorf("Could not create redirect route: %s", err.Error())
		}
		router.Handle(inc, http.StripPrefix(inc[:len(inc)-1], redirect.NewSimpleRedirect(outurl)))
	}

	epc := &ep.EndpointConfig{
		Name:         cfg.Name,
		EndpointType: cfg.EndpointType,
		Bind:         cfg.Bind,
		CertFile:     cfg.CertFile,
		KeyFile:      cfg.KeyFile,
		Router:       router,
	}

	return ep.Build(epc)
}

func main() {

	config, err := ReadConfig()
	if err != nil {
		log.Fatalf("Could not parse config file: %s", err)
	}

	endpoint, err := chainFromConfig(config)
	if err != nil {
		log.Fatalf("Could not build chain: %s", err)
	}

	endpoint.Listen()
}
