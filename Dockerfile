FROM alpine
LABEL author="Victor Harabari" 

RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2
RUN mkdir /balancer
RUN cd /balancer
RUN mkdir config

ADD fancyLoadBalancer /balancer/balancer

WORKDIR /balancer

CMD ["/balancer/balancer"]