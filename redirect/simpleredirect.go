package redirect

import (
	"log"
	"net/http"
	"net/url"
)

// NewSimpleRedirect creates a simple http handler that send redirect header
func NewSimpleRedirect(adr *url.URL) http.HandlerFunc {
	log.Printf("Creating redirect to %s", adr.String())
	return func(w http.ResponseWriter, req *http.Request) {
		target := adr.String() + req.URL.Path
		if len(req.URL.RawQuery) > 0 {
			target += "?" + req.URL.RawQuery
		}
		log.Printf("Redirecting %s to %s", req.RemoteAddr, target)
		http.Redirect(w, req, target, http.StatusTemporaryRedirect)
	}
}
