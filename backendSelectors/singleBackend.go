package backendselectors

// singleBackend will choose each backend once before looping to the beginning
type singleBackend struct {
	backend Backend
}

// NewRoundRobin create a new instance
func newSingleBackend(bsc *BackendSelectorConfig) (BackendSelector, error) {
	return &singleBackend{bsc.Backends[0]}, nil
}

func init() {
	Factories["SingleBackend"] = newSingleBackend
}

func (sb *singleBackend) Select() (Backend, error) {
	return sb.backend, nil
}

func (sb *singleBackend) Copy() BackendSelector {
	return &singleBackend{sb.backend}
}

func (sb *singleBackend) Len() int {
	return 1
}

func (sb *singleBackend) Update(backends []Backend) {
	sb.backend = backends[0]
}
