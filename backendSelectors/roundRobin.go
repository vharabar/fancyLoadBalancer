package backendselectors

import (
	"fancyLoadBalancer/immutable/ring"
	"fmt"
)

// roundRobin will choose each backend once before looping to the beginning
type roundRobin struct {
	data *ring.Ring
}

// NewRoundRobin create a new instance
func newRoundRobin(bsc *BackendSelectorConfig) (BackendSelector, error) {
	if len(bsc.Backends) == 0 {
		return nil, fmt.Errorf("Can't create an empty round robin")
	}
	b := make([]interface{}, len(bsc.Backends))
	for i, v := range bsc.Backends {
		b[i] = v
	}
	return &roundRobin{ring.New(b)}, nil
}

func init() {
	Factories["RoundRobin"] = newRoundRobin
}

func (rr *roundRobin) Select() (Backend, error) {
	return rr.data.Next().(Backend), nil
}

func (rr *roundRobin) Copy() BackendSelector {
	return &roundRobin{rr.data.Copy()}
}

func (rr *roundRobin) Len() int {
	return rr.data.Len()
}

func (rr *roundRobin) Update(backends []Backend) {
	b := make([]interface{}, len(backends))
	for i, v := range backends {
		b[i] = v
	}
	rr.data = ring.New(b)
}
