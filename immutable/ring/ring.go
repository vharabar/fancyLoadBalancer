// Package ring provides a simple immutable ring container
package ring

import (
	"fmt"
)

// Ring is a Ring buffer data structure that loops reading head when it reaches the end
type Ring struct {
	data  []interface{}
	index int
}

// Next returns an item from Ring and moves the curson
func (r *Ring) Next() (item interface{}) {
	item = r.data[r.index]
	r.index = (r.index + 1) % len(r.data)
	return
}

// Peek returns current element without moving the cursor
func (r *Ring) Peek() interface{} {
	return r.data[r.index]
}

// New create a new Ring from list of items
func New(list []interface{}) *Ring {
	if len(list) == 0 {
		panic(fmt.Errorf("Tried to create an empty immutable Ring"))
	}
	r := &Ring{make([]interface{}, len(list)), 0}
	copy(r.data, list)
	return r
}

// Copy return a copy of a Ring
func (r *Ring) Copy() *Ring {
	return New(r.data)
}

// Len returns number of itens in the Ring
func (r *Ring) Len() int {
	return len(r.data)
}
