// Package ring provides a simple immutable ring container
package ring

import (
	"reflect"
	"testing"
)

type any []interface{}

func TestRing_Next(t *testing.T) {
	type fields struct {
		data  []interface{}
		index int
	}
	tests := []struct {
		name      string
		fields    fields
		wantItem  interface{}
		wantIndex int
	}{
		{"Test getting item", fields{any{0, 1}, 0}, 0, 1},
		{"Test looping cursor", fields{any{0, 1}, 1}, 1, 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Ring{
				data:  tt.fields.data,
				index: tt.fields.index,
			}
			if gotItem := r.Next(); !reflect.DeepEqual(gotItem, tt.wantItem) {
				t.Errorf("Ring.Next() = %v, want %v", gotItem, tt.wantItem)
			}
			if r.index != tt.wantIndex {
				t.Errorf("Ring.Index = %v, want %v", r.index, tt.wantIndex)
			}
		})
	}
}

func TestRing_Peek(t *testing.T) {
	type fields struct {
		data  []interface{}
		index int
	}
	tests := []struct {
		name      string
		fields    fields
		want      interface{}
		wantIndex int
	}{
		{"Tets peek", fields{any{0, 1}, 1}, 1, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Ring{
				data:  tt.fields.data,
				index: tt.fields.index,
			}
			if got := r.Peek(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ring.Peek() = %v, want %v", got, tt.want)
			}
			if r.index != tt.wantIndex {
				t.Errorf("Ring.Index = %v, want %v", r.index, tt.wantIndex)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		list []interface{}
	}
	tests := []struct {
		name string
		args args
		want *Ring
	}{
		{"New Ring creation", args{any{0, 1, 2}}, &Ring{any{0, 1, 2}, 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.list); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewError(t *testing.T) {
	type args struct {
		list []interface{}
	}
	tests := []struct {
		name string
		args args
	}{
		{"New Ring creation", args{any{}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if err := recover(); err.(error) == nil {
					t.Error("Creating empty Ring didn't throw error")
				}
			}()
			New(tt.args.list)
		})
	}
}

func TestRing_Copy(t *testing.T) {
	type fields struct {
		data  []interface{}
		index int
	}
	tests := []struct {
		name   string
		fields fields
		want   *Ring
	}{
		{"Copy of a Ring", fields{any{0, 1, 2}, 0}, &Ring{any{0, 1, 2}, 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Ring{
				data:  tt.fields.data,
				index: tt.fields.index,
			}
			if got := r.Copy(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ring.Copy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRing_Len(t *testing.T) {
	type fields struct {
		data  []interface{}
		index int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{"Get length of Ring with 3 elements", fields{any{0, 1, 2}, 0}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Ring{
				data:  tt.fields.data,
				index: tt.fields.index,
			}
			if got := r.Len(); got != tt.want {
				t.Errorf("Ring.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}
